
# Software Engineer Coding Assignment

Coding assignment for a job application.
  - Transaction csv files are read from `transaction_dir`.
  - Production csv files are read from `production_dir`.

### Prerequisites

  1. Python 3.7.2
  2. Flask _(see requirements.txt for version)._

### Installing

  1. [Install Python 3.7.2](https://www.python.org/downloads/).

``` bash
$ Python3 --version
$ Python 3.7.2
```

  2. _(Optional)_ Create VENV:

``` bash
$ virtual venv
$ source venv/bin/activate
```

  3. Install dependencies:

``` bash
$ pip3 install -r requirements.txt
```

## Starting the app

### Prerequisites

  1. Environment variable:

``` bash
$ export APP_SETTINGS="config.DevelopmentConfig"
```

  2. _(Optional)_ Development mode:

``` bash
$ export FLASK_ENV=development
```

  - __NOTE:__ In debug mode, threads are spawned twice (Flask implementation). Check [here](https://stackoverflow.com/questions/43644083/python-thread-running-twice-when-called-once-in-main) for more information.

### Running

  1. Start the app:

``` bash
$ Flask run
 * Environment: development
 * Debug mode: on
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 230-433-240
[2019-02-02 19:32:06,574] INFO in app: Reading: transaction_dir/Transaction_20180101101010.csv
[2019-02-02 19:32:06,586] DEBUG in app: Added: 1, 10, 1000.0, 2019-01-31 10:10:10
[2019-02-02 19:32:06,586] DEBUG in app: Added: 2, 10, 1000.0, 2019-01-31 10:15:10
[2019-02-02 19:32:06,586] DEBUG in app: Added: 3, 20, 2000.0, 2019-01-31 10:15:20
[2019-02-02 19:32:06,586] DEBUG in app: Added: 4, 10, 1000.0, 2018-10-01 10:10:10
[2019-02-02 19:32:06,586] DEBUG in app: Added: 5, 30, 3000.0, 2018-10-01 10:20:10
[2019-02-02 19:32:06,587] DEBUG in app: Added: 6, 20, 2000.0, 2018-10-01 10:15:30
[2019-02-02 19:32:06,587] INFO in app: Reading: product_dir/ProductReference.csv
[2019-02-02 19:32:06,588] DEBUG in app: Added: 10, P1, C1
[2019-02-02 19:32:06,588] DEBUG in app: Added: 20, P2, C1
[2019-02-02 19:32:06,588] DEBUG in app: Added: 30, P3, C2
```

## Web APIs

Open a browser and access the following:

  1. `http://127.0.0.1:5000/assignment/transaction/<transactionId>`

``` json
{
  "productId": 10,
  "transactionAmount": 1000.0,
  "transactionDatetime": "Thu, 31 Jan 2019 10:15:10 GMT",
  "transactionId": 2
}
```
  2. `http://127.0.0.1:5000/assignment/transactionSummaryByManufacturingCity/<last_n_days>`

``` json
{
  "summary": [
    {
      "cityName": "C1",
      "totalAmount": 7000.0
    },
    {
      "cityName": "C2",
      "totalAmount": 3000.0
    }
  ]
}
```

  3. `http://127.0.0.1:5000/assignment/transactionSummaryByProduct/<last_n_days>`

``` json
{
  "summary": [
    {
      "productName": "P1",
      "totalAmount": 3000.0
    },
    {
      "productName": "P2",
      "totalAmount": 4000.0
    },
    {
      "productName": "P3",
      "totalAmount": 3000.0
    }
  ]
}
```
