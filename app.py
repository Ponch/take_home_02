# -*- coding: utf-8 -*-
"""Flask application."""
import os
import threading
import atexit
import csv

from flask import Flask, jsonify

from model import Model
from config import DevelopmentConfig

# Consider: move to config
TIME_INTERVAL = 5

read_transaction_files = []
read_production_files = []
file_reader_thread = None
db = Model()

def create_app():
    """Creates the Flask application."""
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(os.environ['APP_SETTINGS'])

    def cleanup():
        """Cancels any timer thread waiting when application is stopped."""
        global file_reader_thread
        file_reader_thread.cancel()

    def initialize_thread():
        """Starts the file_reader timer thread."""
        global file_reader_thread
        file_reader_thread = threading.Timer(TIME_INTERVAL, load_files, ())
        file_reader_thread.start()

    def load_files():
        """Loads transaction and production files to memory. Already loaded files
        are saved in a references to avoid rereading again. Restarts another timer
        thread after checking."""
        global file_reader_thread
        _load_files(app.config['TRANSACTION_DIR'], 'transaction')
        _load_files(app.config['PRODUCT_DIR'], 'production')

        file_reader_thread = threading.Timer(TIME_INTERVAL, load_files, ())
        file_reader_thread.start()

    def _load_files(directory, file_type):
        """Reads csv files from directory and load them to memory.

        Args:
            directory: filepath where to read csv files.
            file_type: type of files inside the directory. Supported values: 'transaction' or
        'production'.

        Raises:
            ValueError if file_type is not supported.
        """
        global db

        # Consider refactoring: file_type is checked twice:
        #   - Use 1 list to store read files?
        if file_type == 'transaction':
            global read_transaction_files
            read_files = read_transaction_files
        elif file_type == 'production':
            global read_production_files
            read_files = read_production_files
        else:
            raise ValueError('file_type value is not supported:' \
                + file_type)

        for filename in os.listdir(directory):

            if filename in read_files:
                continue

            filepath = os.path.join(directory, filename)
            app.logger.info('Reading: ' + filepath)

            with open(filepath, 'r') as fd:
                csv_reader = csv.reader(fd)
                next(csv_reader) # skip headers

                for row in csv_reader:

                    if file_type == 'transaction':
                        ret = db.add_transaction(*_get_transaction_values(row))
                    elif file_type == 'production':
                        ret = db.add_production(*_get_production_values(row))
                    else:
                        raise ValueError('file_type value is not supported:' \
                            + file_type)

                    if ret:
                        app.logger.debug('Added: ' + ','.join(row))
                    else:
                        app.logger.debug('Failed to add: ' + ','.join(row))


            read_files.append(filename)


    def _get_production_values(row):
        """Removes whitespaces and coverts production values to correct data type.

        Args:
            row: list object containg values from read csv line.

        Returns:
            tuple containing: product_id, product_name, product_manufacturing_city
        with the correct data type.
        """
        product_id = int(row[0])
        product_name = row[1].strip()
        product_manufacturing_city = row[2].strip()

        return (product_id, product_name, product_manufacturing_city)

    def _get_transaction_values(row):
        """Removes whitespaces and coverts transaction values to correct data type.

        Args:
            row: list object containg values from read csv line.

        Returns:
            tuple containing: transaction_id, product_id, transaction_amount,
        transaction_datetime with the correct data type.
        """
        transaction_id = int(row[0])
        product_id = int(row[1])
        transaction_amount = float(row[2])

        # Consider converting string to datetime here instead.
        transaction_datetime = row[3].strip()

        return (transaction_id, product_id, transaction_amount, transaction_datetime)

    @app.route('/assignment/transaction/<transactionId>', methods=['GET'])
    def get_transaction(transactionId):
        """Gets transaction information of transactionId.

        Args:
            transactionId: Transaction id of transaction to get info.

        Returns:
            Flast json response object:
            {
              "productId": 10,
              "transactionAmount": 1000.0,
              "transactionDatetime": "Thu, 31 Jan 2019 10:15:10 GMT",
              "transactionId": 2
            }
        """
        global db
        return jsonify(db.get_transaction_by_id(int(transactionId)))

    @app.route('/assignment/transactionSummaryByProduct/<last_n_days>', methods=['GET'])
    def get_transaction_summary_by_product(last_n_days):
        """Gets transactions that occurred in the last N days and grouped by product
        name.

        Args:
            last_n_days: number of days in the past when to check the transactions.

        Returns:
            Flast json response object:
            {
              "summary": [
                {
                  "productName": "P1",
                  "totalAmount": 3000.0
                },
                {
                  "productName": "P2",
                  "totalAmount": 4000.0
                }
              ]
            }
        """
        global db
        return jsonify(db.get_transaction_summary_by_product_name(int(last_n_days)))

    @app.route('/assignment/transactionSummaryByManufacturingCity/<last_n_days>', methods=['GET'])
    def get_transaction_summary_by_city(last_n_days):
        """Gets transactions that occurred in the last N days and grouped by
        manufacturing city.

        Args:
            last_n_days: number of days in the past when to check the transactions.

        Returns:
        {
          "summary": [
            {
              "cityName": "C1",
              "totalAmount": 7000.0
            },
            {
              "cityName": "C2",
              "totalAmount": 3000.0
            }
          ]
        }
        """
        global db
        return jsonify(db.get_transaction_summary_by_city(int(last_n_days)))

    initialize_thread()
    atexit.register(cleanup)

    return app

if __name__ == '__main__':
    app = create_app()
    app.run()
