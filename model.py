# -*- coding: utf-8 -*-
"""Model"""
import threading

from datetime import datetime, timedelta

class Transaction():
    """Class for transaction record."""
    def __init__(self, transaction_id, product_id, transaction_amount,
        transaction_datetime):
        """Initializer.

        Args:
            transaction_id: id.
            product_id: production id.
            transaction_amount: amount.
            transaction_datetime: date time object.
        """

        self.transaction_id = transaction_id
        self.product_id = product_id
        self.transaction_amount = transaction_amount
        self.transaction_datetime = transaction_datetime

    def serialize(self):
        """Returns a dict containing the transaction information."""
        return {
            'transactionId' : self.transaction_id,
            'productId' : self.product_id,
            'transactionAmount' : self.transaction_amount,
            'transactionDatetime' : self.transaction_datetime
        }

class Production():
    """Class for production record."""
    def __init__(self, product_id, product_name, product_manufacturing_city):
        """Initializer.

        Args:
            product_id: product id.
            product_name: name of the product.
            product_manufacturing_city: city.
        """
        self.product_id = product_id
        self.product_name = product_name
        self.product_manufacturing_city = product_manufacturing_city

    def serialize(self):
        """Returns a dict containing the production information."""
        return {
            'productId' : self.product_id,
            'productName' : self.product_name,
            'productManufacturingCity' : self.product_manufacturing_city
       }


class Model():
    """Model layer."""
    def __init__(self):
        """Initializer"""
        self._lock = threading.Lock()
        self._transactions = {}
        self._productions = {}

    def add_transaction(self, transaction_id, product_id, transaction_amount,
        transaction_datetime):
        """Adds a transaction.

        Args:
            transaction_id: id.
            product_id: production id.
            transaction_amount: amount.
            transaction_datetime: date time object.

        Returns:
            True if success, otherwise false (datetime parse failed).
        """

        try:
            # 2018-10-01 10:10:10
            transaction_datetime_obj = datetime.strptime(transaction_datetime,
                '%Y-%m-%d %H:%M:%S')
        except ValueError:
            return False

        with self._lock:
            self._transactions[transaction_id] = Transaction(transaction_id,
                product_id, transaction_amount, transaction_datetime_obj)

        return True

    def add_production(self, product_id, product_name, product_manufacturing_city):
        """Adds a production record.

        Args:
            product_id: product id.
            product_name: name of the product.
            product_manufacturing_city: city.

        Returns:
            True if success, otherwise false.
        """
        with self._lock:
            self._productions[product_id] = Production(product_id, product_name,
                    product_manufacturing_city)

        return True

    def get_transaction_by_id(self, transaction_id):
        """Retrieves the transaction information of the transaction_id.

        Args:
            transaction_id: Transaction_id of the corresponding transaction to get
        information.

        Return:
            Dict object containing the transaction information. Returns empty dict
        if not found.
        """
        with self._lock:
            try:
                return self._transactions[transaction_id].serialize()
            except KeyError:
                return {}

    def get_transaction_summary_by_city(self, last_n_days):
        """Collects last transactions that happened for the last_n_days and groups
        them by city.

        Args:
            last_n_days: Number of days in the past.

        Return:
            dictionary where key is city and value is the transactions.
        """
        with self._lock:
            transactions_within_date = self._get_transactions_by_n_days(last_n_days, 'city')

            # serialize
            summary = []
            for k, v in transactions_within_date.items():
                summary.append({
                    'cityName' : k,
                    'totalAmount' : v
                })

            return {'summary' : summary}

    def get_transaction_summary_by_product_name(self, last_n_days):
        """Collects last transactions that happened for the last_n_days and groups
        them by product name.

        Args:
            last_n_days: Number of days in the past.

        Return:
            dictionary where key is product_name and value is the transactions.
        """
        with self._lock:
            transactions_within_date = self._get_transactions_by_n_days(last_n_days, 'name')

            # serialize
            summary = []
            for k, v in transactions_within_date.items():
                summary.append({
                    'productName' : k,
                    'totalAmount' : v
                })

            return {'summary' : summary}

    def _get_transactions_by_n_days(self, last_n_days, group_by):
        """Collects last transactions that happened for the last_n_days and groups
        them by group_by.

        Args:
            last_n_days: Number of days in the past.
            group_by: grouping to be done. Supported values: city or name.

        Return:
            dictionary where key is group_by and value is the transactions.

        Raises:
            ValueError if group_by is not supported.
        """
        transactions_within_date = {}
        now = datetime.now()

        for k, v in self._transactions.items():
            if now - timedelta(days = last_n_days) <= v.transaction_datetime <= now:
                if group_by == 'city':
                    grouping = self._get_city_by_id(v.product_id)
                elif group_by == 'name':
                    grouping = self._get_product_name_by_id(v.product_id)
                else:
                    raise ValueError('Unsuppported value for group_by: ' + group_by)

                try:
                    transactions_within_date[grouping] += v.transaction_amount
                except KeyError:
                    transactions_within_date[grouping] = v.transaction_amount

        return transactions_within_date

    def _get_product_name_by_id(self, product_id):
        """Retrives corresponding product_name of product_id.

        Args:
            product_id: product_id of the product_name to be retrieved.

        Return:
            product_name of the corresponding product_id. Returns empty string
        if not found.
        """
        try:
            return self._productions[product_id].product_name
        except KeyError:
            return ''

    def _get_city_by_id(self, product_id):
        """Retrives corresponding manufacturing_city of product_id.

        Args:
            product_id: product_id of the manufacturing_city to be retrieved.

        Return:
            manufacturing_city of the corresponding product_id. Returns empty string
        if not found.
        """
        try:
            return self._productions[product_id].product_manufacturing_city
        except KeyError:
            return ''




